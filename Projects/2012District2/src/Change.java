import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;

public class Change {

	static int[] vals = {1, 5, 10, 25, 50};
	
	static HashMap<Integer, Integer> mem = new HashMap<Integer, Integer>();
	
	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("change.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			combos = new HashSet<ChangeCombo>();
			int n = rd.nextInt();
			rd.nextLine();
			int m = countChangeCombos(n, 0, new ChangeCombo());
			System.out.println(m);
		}
	}
	
	static Set<ChangeCombo> combos;
	
	private static int countChangeCombos(int total, int c, ChangeCombo combo) {
//		System.out.println(total + " "+ c);
		
		if (!combos.contains(combo) && total == 0) {
			combos.add(combo);
			return c;
		}
		else if (total < 0) {
			return 0;
		}
		else if (mem.containsKey(total)) {
			return mem.get(total);
		}
		
		int s = 0;
		
		for (int i = 0; i < vals.length; i++) {
			s += countChangeCombos(total - vals[i], c + 1, combo.inc(i));
		}
		mem.put(total, s);
		return s;
		
	}

}

class ChangeCombo {
	int[] counts = new int[5];
	
	public ChangeCombo() {
		
	}
	
	public ChangeCombo(ChangeCombo other) {
		for (int i = 0; i < other.counts.length; i++) {
			counts[i] = other.counts[i];
		}
	}
	
	public ChangeCombo inc(int i) {
		counts[i]++;
		return this;
	}
	
	public int hashCode() {
		return Objects.hash(Arrays.toString(counts));
	}
	
	public boolean equals(Object other) {
		return this.hashCode() == other.hashCode();
	}
}
