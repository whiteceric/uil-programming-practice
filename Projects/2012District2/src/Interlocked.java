import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Interlocked {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("interlocked.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			String a = rd.next();
			String b = rd.next();
			String c = rd.next();
			rd.nextLine();
			
			
//			System.out.println(a + " " + b + " " + c);
			boolean good = good(a, c) && good(b, c);
			System.out.println((good) ? "YES" : "NO");
//			break;
		}
		
	}
	
	private static boolean good(String a, String b) {
		int i = 0;
		int j = 0;
		while (i < a.length() && j < b.length()) {
//			System.out.println(i + " - " + j);
			if (b.charAt(j) == a.charAt(i)) {
				i++;
//				System.out.println("GOT IT");
			}
			j++;
		}
//		System.out.println("END: " + i);
		
		return i == a.length();
	}

}
