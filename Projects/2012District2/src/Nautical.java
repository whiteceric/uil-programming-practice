import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Nautical {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("nautical.dat"));
		rd.nextLine();
		while (rd.hasNextLine()) {
			int n = Integer.parseInt(rd.nextLine());
			System.out.printf("%.1f\n",  n * 1.15);
		}
		
	}

}
