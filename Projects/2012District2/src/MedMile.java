import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.TreeMap;

public class MedMile {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("medmile.dat"));
		
		rd.nextLine();
		
//		String time = "21:12.32";
//		System.out.println(getTime(time));
//		System.out.println(getTimeStr(1272.32));
		
		while (rd.hasNextLine()) {
			Scanner line = new Scanner(rd.nextLine());
			String name = line.next();
			TreeMap<Double, String> times = new TreeMap<Double, String>();
			
			while (line.hasNext()) {
				String timeStr = line.next();
				times.put(getTime(timeStr), timeStr);
			}
			
			double[] nums = new double[times.size()];
			int c = 0;
			for (double d : times.keySet()) {
				nums[c++] = d;
			}
			
			double med = -1;
			if (nums.length % 2 == 0) {
				med = (nums[nums.length / 2] + nums[nums.length / 2 - 1]) / 2;
			}
			else {
				med = nums[nums.length / 2];
			}
			System.out.println(name + " " + getTimeStr(med));
			
		}
		
	}
	
	private static String getTimeStr(double time) {
		String str = "";
//		System.out.println(time);
		int mm = (int) (time / 60);
		time *= 100;
		double sec = ((int) time - mm * 6000) / 100.0;
//		System.out.println(mm + " -- " + sec);
		str = mm + ":" +  ((sec < 10) ? "0" : "") + sec +"00000";
//		if (str.substring(str.indexOf('.')).length() != 2) {
//			str += "0";
//		}
		return str.substring(0, 8);
	}
	
	private static double getTime(String str) {
		double mm = Double.parseDouble(str.substring(0, str.indexOf(":"))) * 60;
		mm += Double.parseDouble(str.substring(str.indexOf(":") + 1, str.indexOf('.')));
		mm += Double.parseDouble(str.substring(str.indexOf('.') + 1)) * .01;
		return mm;
	}

}
