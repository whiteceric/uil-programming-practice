import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Binary {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("binary.dat"));
		
		int sets = Integer.parseInt(rd.nextLine());
		
		for (int q = 0; q < sets; q++) {
			boolean[][] mat = new boolean[4][6];
			
			for (int i = 0; i < mat.length; i++) {
				String str = rd.nextLine();
				for (int j = 0; j < mat[0].length; j++) {
					mat[i][j] = str.charAt(j) == '*';
				}
			}
			
			for (int i = 0; i < 6; i++) {
				int n = getColVal(mat, i);
				System.out.print(n);
				if (i != 5 && i % 2 == 1) {
					System.out.print(":");
				}
			}
			System.out.println();
			
			
		}
		
	}
	
	private static int getColVal(boolean[][] a, int c) {
		String num = "";
		for (int r = 0; r < a.length; r++) {
			num += (a[r][c]) ? "1" : "0";
		}
		return Integer.parseInt(num, 2);
	}

}
