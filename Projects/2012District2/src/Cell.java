import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Cell {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("cell.dat"));
		int a = 0;
		int b = 0;
		
		while (rd.hasNextLine()) {
			double d = rd.nextDouble();
			rd.nextLine();
			a += Math.floor(d);
			b += Math.ceil(d);
		}
		
		System.out.println("PLAN A: " + a);
		System.out.println("PLAN B: " + b);
		
		
	}

}
