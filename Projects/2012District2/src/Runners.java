import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Runners {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("runners2.dat"));
		
		int sets = rd.nextInt();
		rd.nextLine();
		
		for (int q = 0; q < sets; q++) {
			int rows = rd.nextInt();
			int cols = rd.nextInt();
			rd.nextLine();
			Node[][] room = new Node[rows][cols];
			Node russ = null, seth = null, tom = null, prob = null, judge = null;
			
			for (int i = 0; i < rows; i++) {
				String line = rd.nextLine();
				for (int j = 0; j < cols; j++) {
					room[i][j] = new Node(i, j, line.charAt(j));
					switch (line.charAt(j)) {
						case 'R': russ = room[i][j]; break;
						case 'S': seth = room[i][j]; break;
						case 'T': tom = room[i][j]; break;
						case 'P': prob = room[i][j]; break;
						case 'J': judge = room[i][j]; break;
					}
				}
			}
			
			int russD = Integer.MAX_VALUE;
			int sethD = Integer.MAX_VALUE;
			int tomD = Integer.MAX_VALUE;
			
			for (Node n : prob.getAdjList(room)) {
				if (n.dat != '.') continue;
				int rnj = findShortestPath(russ, n, room) + findShortestPath(n, judge, room);
				int snj = findShortestPath(seth, n, room) + findShortestPath(n, judge, room);
				int tnj = findShortestPath(tom, n, room) + findShortestPath(n, judge, room);
				
				if (rnj < russD) {
					russD = rnj;
				}
				if (snj < sethD) {
					sethD = snj;
				}
				if (tnj < tomD) {
					tomD = tnj;
				}
			}
			
			Set<String> out = new TreeSet<String>();
			int min = Math.min(russD, Math.min(sethD, tomD));
			if (russD == min) out.add("RUSSELL");
			if (sethD == min) out.add("SETH");
			if (tomD == min) out.add("THOMAS");
			
			System.out.print(min);
			for (String s : out) {
				System.out.print(" " + s);
			}
			System.out.println();
//			break;
		}
		
	}
	
	private static int findShortestPath(Node start, Node end, Node[][] room) {
		for (Node[] a : room) {
			for (Node n : a) {
				n.dist = Integer.MAX_VALUE;
			}
		}
		
		start.dist = 0;
		
		PriorityQueue<Node> que = new PriorityQueue<Node>();
		que.add(start);
		while (!que.isEmpty()) {
			Node node = que.remove();
			
			if (node.dat == end.dat && node.r == end.r && node.c == end.c) {
				break;
			}
			
			List<Node> adj = node.getAdjList(room);
			
			for (Node n : adj) {
				if (n.dat == '.' || n.dat == end.dat) {
					int nd = node.dist + 1;
					if (nd < n.dist) {
						que.add(n);
						n.dist = nd;
					}
				}
			}
		}
		
		return end.dist;
	}
	

}

class Node implements Comparable<Node> {
	int r, c;
	char dat;
	int dist;
	
	Node(int r_, int c_, char d_) {
		r = r_;
		c = c_;
		dat = d_;
	}
	
	List<Node> getAdjList(Node[][] room) {
		List<Node> list = new ArrayList<Node>();
		if (r > 0) {
			list.add(room[r-1][c]);
		}
		if (c > 0) {
			list.add(room[r][c-1]);
		}
		
		if (r < room.length-1) {
			list.add(room[r+1][c]);
		}
		
		if (c < room[0].length-1) {
			list.add(room[r][c+1]);
		}
		
		return list;
	}

	@Override
	public int compareTo(Node o) {
		return dist - o.dist;
	}
	
	
}
