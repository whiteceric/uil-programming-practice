import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Family {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("family.dat"));
		
		int sets = rd.nextInt();
		rd.nextLine();
		
		for (int q = 0; q < sets; q++) {
			char[][] pic = new char[12][18];
			
			for (int i = 0; i < 18; i++) {
				String line = rd.nextLine();
				for (int j = 0; j < 12; j++) {
					pic[j][i] = line.charAt(j);
				}
			}
			if (rd.hasNextLine()) {
				rd.nextLine();
			}
			
			if (q > 0) {
				System.out.println();
			}
			
			for (int i = 0; i < 12; i++) {
				for (int j = 17; j > -1; j--) {
					System.out.print(pic[i][j]);
				}
				System.out.println();
			}
		}
		
	}

}
