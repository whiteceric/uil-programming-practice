import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Threes {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("threes.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			int x = Integer.parseInt(rd.nextLine());
			int m = x;
			
			while (!good(m)) {
				m += x;
			}
			
			System.out.println(m);
			
		}
		
	}
	
	private static boolean good(int n) {
		String str = "" + n;
		for (char c : str.toCharArray()) {
			if (c != '3' && c != '0') {
				return false;
			}
		}
		return true;
	}

}
