import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Spring {

	static double packValue = 0.48 * 14;
	static double tubValue = 0.45 * 15;
	
	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("spring.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			String student = rd.next();
			double total = 0;
			int lines = rd.nextInt();
			rd.nextLine();
			for (int i = 0; i < lines; i++) {
				Scanner line = new Scanner(rd.nextLine());
				
				while (line.hasNext()) {
					String tok = line.next();
					int q = Integer.parseInt(tok.substring(1));
					if (tok.startsWith("T")) {
						total += q * tubValue;
					}
					else if (tok.startsWith("P")) {
						total += q * packValue;
					}
					else if (tok.startsWith("D")){
						total += q;
					}
				}
			}
			System.out.print(student + " ");
			if (total > 500) {
				total -= 500;
				System.out.print("OVER ");
			}
			else {
				total = 500 - total;
			}
			
			System.out.printf("$%.2f\n", total);
		}
		
	}

}
