import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Clock {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("clock.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			String[] line = rd.nextLine().split(":");
			int hour = Integer.parseInt(line[0]);
			int min  = Integer.parseInt(line[1]);
			
			if (min == 0) {
				hour = 12 - hour;
				System.out.println(hour + ":00");
			}
			else {
				hour = 11 - hour;
				min = 60 - min;
				System.out.println(hour + ":" + ((min < 10) ? "0" : "") + min);
			}		
		}
		
	}

}
