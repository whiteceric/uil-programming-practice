import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Digicount {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("digicount.dat"));
		
		while (rd.hasNext()) {
			int num = rd.nextInt();
			int[] counts = new int[10];
			for (int i = 1; i <= num; i++) {
				int n = i;
//				System.out.println("--" + i);
				while (n > 0) {
					counts[n % 10]++;
					n /= 10;
//					System.out.println(n);
				}
			}
			
			System.out.println(Arrays.toString(counts).replaceAll(",", "").replaceAll("\\]", "").replaceAll("\\[", ""));
//			break;
		}
		
		
	}

}
