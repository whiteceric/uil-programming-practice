import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

public class XML {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("xml.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			int lines = rd.nextInt();
			rd.nextLine();
			
			Stack<String> stack = new Stack<String>();
			
			boolean valid = true;
			
			for (int i = 0; i < lines; i++) {
				String str = rd.nextLine();
				if (str.startsWith("</")) {
					str = str.replaceAll("[<>/]", "");
					if (!stack.peek().equals(str)) {
						valid = false;
					}
					stack.pop();
				}
				else if (str.startsWith("<")) {
					stack.push(str.replaceAll("[<>]", ""));
				}
			}
			if (!stack.isEmpty()) {
				valid = false;
			}
			
			System.out.println((valid) ? "valid" : "invalid");
		
		}
		
	}

}
