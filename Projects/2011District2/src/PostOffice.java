import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class PostOffice {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("postoffice.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			double total = 0;
			Scanner line = new Scanner(rd.nextLine());
			while (line.hasNext()) {
				total += line.nextDouble();
			}
			
			System.out.println((total < 5) ? "OK" : "OVERWEIGHT");
		}
		
	}

}
