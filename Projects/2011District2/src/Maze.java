import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Maze {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("maze2.dat"));
		
		int sets = Integer.parseInt(rd.nextLine());
		
		for (int q = 0; q < sets; q++) {
			char[][] maze = new char[20][19];
			int startR = -1, startC = -1, endR = -1, endC = -1;
			
			for (int i = 0; i < 20; i++) {
				String row = rd.nextLine();
				for (int j = 0; j < 19; j++) {
					maze[i][j] = row.charAt(j);
					if (maze[i][j] == 'S') {
						startR = i;
						startC = j;
					}
					else if (maze[i][j] == 'E') {
						endR = i;
						endC = j;
					}
				}
			}
			if (q < sets-1)
				rd.nextLine();
			
			boolean end = findEnd(startR, startC, maze, endR, endC);
			System.out.println("Maze #" + (q+1) + ": " + ((end) ? "YES" : "NO"));
			
		}
		
	}
	
	private static boolean findEnd(int row, int col, char[][] a, int endRow, int endCol) {
		if (row < 0 || row >= 20 || col < 0 || col >= 19 || a[row][col] == '#') {
			return false;
		}
		
		if (row == endRow && col == endCol) {
			return true;
		}
		a[row][col] = '#';
		return findEnd(row+1, col, a, endRow, endCol) ||
				findEnd(row-1, col, a, endRow, endCol) ||
				findEnd(row, col+1, a, endRow, endCol) ||
				findEnd(row, col-1, a, endRow, endCol);
	}

}
