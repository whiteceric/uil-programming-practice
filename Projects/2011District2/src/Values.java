import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Values {

	static String vals = " ";
	
	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("values.dat"));
		ArrayList<Word> words = new ArrayList<Word>();	
		
		for (char c = 'A'; c <= 'Z'; c++) {
			vals += c;
		}
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			String w = rd.nextLine();
			int val = getVal(w);
//			System.out.println(w + " " + val);
			words.add(new Word(w, val));
		}
		
		Collections.sort(words);
		
		for (Word w : words) {
			System.out.println(w.val + " " + w.word);
		}
		
	}
	
	private static int getVal(String str) {
		int total = 0;
		
		for (char c : str.toCharArray()) {
			total += vals.indexOf(c);
		}
		
		return total;
	}

}

class Word implements Comparable<Word> {

	String word;
	int val;
	
	Word(String w, int v) {
		word = w;
		val = v;
	}
	
	@Override
	public int compareTo(Word other) {
		if (other.val == val) {
			return word.compareTo(other.word);
		}
		return val - other.val;
	}
	
}
