import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class BlobCount {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("blobcount.dat"));
		//
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			int rows = rd.nextInt();
			int cols = rd.nextInt();
			int cases = rd.nextInt();
			rd.nextLine();
			
			 boolean[][] a = new boolean[rows][cols];
			 for (int i = 0; i < rows; i++) {
				 String line = rd.nextLine();
				 for (int j = 0; j < cols; j++) {
					 if (line.charAt(j) == '*') {
						 a[i][j] = true;
					 }
				 }
			 }
			 
			 for (int t = 0; t < cases; t++) {
				 int r = rd.nextInt() - 1;
				 int c = rd.nextInt() - 1;
				 rd.nextLine();
				 
				 int w = 0, h = 0;
				 for (int i = c; i < cols; i++) {
					 if (a[r][i]) {
						 w++;
					 }
					 else {
						 break;
					 }
				 }
				 
				 for (int i = r; i < rows; i++) {
					 if (a[i][c]) {
						 h++;
					 }
					 else {
						 break;
					 }
				 }
				 
				 int size = w * h;
				 if (size == 0) {
					 System.out.println("NO BLOB");
				 }
				 else {
					 System.out.println(size);
				 }
			 }
		}
		
	}

}
