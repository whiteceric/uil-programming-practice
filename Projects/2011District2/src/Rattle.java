import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Rattle {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("rattle.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			String msg = rd.nextLine();
			int mLen = (int) Math.sqrt(msg.length());
			
			char[][] matrix = new char[mLen][mLen];
			
			int index = 0;
			for (int i = 0; i < mLen; i++) {
				for (int j = 0; j < mLen; j++) {
					matrix[i][j] = msg.charAt(index++);
				}
			}
			
			// rotations up
			for (int i = 0; i < mLen; i += 2) {
				char first = matrix[0][i];
				for (int r = 0; r < mLen - 1; r++) {
					matrix[r][i] = matrix[r+1][i];
				}
				matrix[mLen-1][i] = first;
			}
			
			// rotate down
			for (int i = 1; i < mLen; i += 2) {
				char last = matrix[mLen-1][i];
				for (int r = mLen - 1; r > 0; r--) {
					matrix[r][i] = matrix[r-1][i];
				}
				matrix[0][i] = last;
			}
			
			PRINT: for (char[] a : matrix) {
				for (char c : a) {
					if (c == '*') {
						break PRINT;
					}
					System.out.print(c);
				}
			}
			System.out.println();
		}
		
	}

}
