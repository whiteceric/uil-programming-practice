import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Pals {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("pals.dat"));
		
		rd.nextLine();
		
		while (rd.hasNextLine()) {
			int m = rd.nextInt();
			int h = m;
			int d = rd.nextInt();
//			System.out.println(m + "  " + d);
			int t = 0;
			rd.nextLine();
			
			while (("" + h).length() <= d) {
				if (("" + h).length() == d && rev("" + h).equals("" + h)) {
					t++;
				}
				h += m;
//				System.out.println(h);
			}
			
			System.out.println(t);
		}
		
	}
	
	private static String rev(String str) {
		return new StringBuilder(str).reverse().toString();
	}

}
