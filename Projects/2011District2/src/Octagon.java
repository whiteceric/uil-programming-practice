import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Octagon {

	public static void main(String[] args) throws IOException {
		Scanner rd = new Scanner(new File("octagon.dat"));
		
		while (rd.hasNext()) {
			int n = rd.nextInt();
			
			for (int i = 0; i < n - 1; i++) {
				System.out.print(" ");
			}
			
			for (int i = 0; i < n; i++) {
				System.out.print("x");
			}
			System.out.println();
			for (int i = 0; i < n-1; i++) {
				for (int j = 0; j < n - i - 2; j++) {
					System.out.print(" ");
				}
				System.out.print("x");
				for (int j = 0; j < n + 2 * i; j++) {
					System.out.print(" ");
				}
				System.out.println("x");
			}
			
			for (int i = 0; i < n - 2; i++) {
				System.out.print("x");
				for (int j = 0; j < n + 2 * (n-2); j++) {
					System.out.print(" ");
				}
				System.out.println("x");
			}
			
			for (int i = 0; i < n-1; i++) {
				for (int j = 0; j < i; j++) {
					System.out.print(" ");
				}
				System.out.print("x");
				for (int j = 0; j < n + 2 * (n - i - 2); j++) {
					System.out.print(" ");
				}
				System.out.println("x");
			}
			
			for (int i = 0; i < n - 1; i++) {
				System.out.print(" ");
			}
			
			for (int i = 0; i < n; i++) {
				System.out.print("x");
			}
			System.out.println("\n");
			
//			break;
		}
		
	}

}
